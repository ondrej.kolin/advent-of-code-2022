use std::fs::File;
use std::io::{prelude::*, BufReader};

fn main() -> std::io::Result<()> {
    // Max calories for this elves' group
    let mut max = vec![-1, -1, -1];
    // What's the current carrying
    let mut current = 0;
    // Counter
    let mut counter = 0;

    let file = File::open("/home/ondrej/Downloads/advent_of_code_1")?;
    let lines = BufReader::new(file).lines();
    for line in lines {
        if let Ok(ln) = line {
            // It's a elf ending
            if ln.trim() == "" {
                println!("Elf #{} - {}", counter, current);
                // position this elf
                let mut desired_position = max.len();
                for (index, value) in max.iter().enumerate() {
                    if &current >= value {
                        desired_position = index
                    }
                }
                while desired_position < max.len() {
                    let old_max = max[desired_position];
                    max[desired_position] = current;
                    current = old_max;
                    if desired_position != 0 {
                        desired_position -= 1;
                    }
                    else {
                        break
                    }
                }
                //println!("Move to {} - {}", desired_location, current);
                // Start a new current value
                current = 0;
                counter += 1;
                
            }
            // This elf gets a value
            else {
                current += i32::from_str_radix(&ln, 10).expect("Failed to parse the int!");
            }
        }
    }
    let mut sum = 0;
    for (index, value) in max.iter().enumerate() {
        println!("[{}] - {}", index, value);
        sum += value
    }
    println!("Sum is: {}", sum);
    
    
    Ok(())
}
