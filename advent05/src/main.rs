use std::fs::File;
use std::io::Read;
use itertools::Itertools;
use std::collections::VecDeque;
use regex::Regex;

pub struct Storage {
    columns: Vec<VecDeque<char>>
}

impl Storage {
    fn load_line_crate(&mut self, index: usize, entry: String) {
        if !entry.trim().is_empty() {
            let c = entry.trim().chars().nth(1).unwrap();
            self.columns[index].push_front(c);
        }
    }

    fn load_line(& mut self, line: &str) {
        let line_columns_count = (line.len() + 1) / 4;
        for _ in self.columns.len()..line_columns_count { 
            self.columns.push(VecDeque::new());
        }
        line.chars()
            .chunks(4)
            .into_iter()
            .map(|x| x.collect::<String>())
            .enumerate()
            .for_each(|(index, entry)| self.load_line_crate(index, entry));
    }

    fn move_box(&mut self, from: usize, to: usize) {
        let c = self.columns[from].pop_back().unwrap();
        self.columns[to].push_back(c);
    }

    fn move_boxes(& mut self, from:usize, to:usize, count: usize) {
        for _ in 0..count {
            self.move_box(from, to);
        }
    }

    fn visualize(self: &Self) {
        for (index, col) in self.columns.iter().enumerate() {
            println!("{index} {}", col.into_iter().collect::<String>())
        }
        println!("{}", "-".repeat(self.columns.len()));
    }

    fn visualize_res(self: &Self) {
        for col in self.columns.iter() {
            print!("{}", col.back().unwrap_or(&'-'));
        }
        println!("");
    }

    fn new() -> Self {
        Self{
            columns: vec![]
        }
    }
}
fn main() {
    let mut storage = Storage::new();
    let mut f = File::open("input.txt").unwrap();
    let mut buf = String::new();
    f.read_to_string(&mut buf).unwrap();
    let mut lines = buf.lines();
    loop {
        let line = lines.next().unwrap();
        //Special lines
        if line.is_empty() {
            println!("End of crates");
            break;
        }
        if line.replace(&['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'][..], "").trim().is_empty() {
            println!("Is the index line, not interested in that");
            continue;
        }
        // Do the storage loading
        storage.load_line(line);
    }
    storage.visualize();
    // Now let's do the moves
    let re = Regex::new(r"move (\d+) from (\d+) to (\d)").unwrap();
    for command in lines {
        let matches = re.captures(command).unwrap();
        let (count, from, to) = [
            matches.get(1),
            matches.get(2),
            matches.get(3),
            ].iter().map(|val| val
                .unwrap()
                .as_str()
                .to_string()
                .parse::<usize>()
                .unwrap()).collect_tuple().unwrap();
        storage.move_boxes(from -1, to -1, count);
        println!("{}", matches.get(0).unwrap().as_str());
        storage.visualize();
        println!("Last line: ");
        storage.visualize_res();
    }
}
