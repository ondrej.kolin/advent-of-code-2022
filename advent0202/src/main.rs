use std::{fmt, fs::File, io::Read};

pub enum GameResult {
    X,
    Y,
    Z,
}

impl GameResult {
    fn from_str(value: &str) -> Option<Self> {
        match value.trim() {
            "X" => Some(Self::X),
            "Y" => Some(Self::Y),
            "Z" => Some(Self::Z),
            _ => None
        }
    }

    fn points(self) -> u8 {
        match self {
            Self::X => 0,
            Self::Y => 3,
            Self::Z => 6,
        }
    }

    fn find_me(self: &Self, him: Him) -> Me {
        match self {
            // I should lose
            Self::X => match him {
                Him::A => Me::Scissors,
                Him::B => Me::Rock,
                Him::C => Me::Paper
            }
            // I should draw
            Self::Y => match him {
                Him::A => Me::Rock,
                Him::B => Me::Paper,
                Him::C => Me::Scissors,
            }
            // I should win
            Self::Z => match him {
                Him::A => Me::Paper,
                Him::B => Me::Scissors,
                Him::C => Me::Rock
            }
        }
    }
}
pub enum Me {
    Rock,
    Paper,
    Scissors,
}

impl Me {
    fn points(self: &Self) -> u8{
        match self {
            Self::Rock => 1,
            Self::Paper => 2,
            Self::Scissors => 3
        }
    }
}

pub enum Him {
    A,
    B,
    C,
}

impl Him {
    fn from_str(value: &str) -> Option<Him> {
        match value.trim() {
            "A" => Some(Him::A),
            "B" => Some(Him::B),
            "C" => Some(Him::C),
            _ => None
        }
    }
}

#[derive(Debug)]
enum LineProcessError {
    EmptyLine,
    ParseError,
    ValueError,
}

impl fmt::Display for LineProcessError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            LineProcessError::ParseError => write!(f, "Unable to parse the line!"),
            LineProcessError::EmptyLine => write!(f, "It's an empty line"),
            LineProcessError::ValueError => write!(f, "Value error!")
        }
    }
}

fn process_row(value: String) -> Result<u8, LineProcessError> {
    let cleaned = value.trim();
    if cleaned.is_empty() {
        return Err(LineProcessError::EmptyLine);
    }
    let mid = cleaned.find(" ");
    let splitter = match mid {
        None => return Err(LineProcessError::ParseError),
        Some(mid) => cleaned.split_at(mid)
    };
    let him = match Him::from_str(splitter.0) {
        Some(value) => value,
        None => return Err(LineProcessError::ValueError)
    };
    let game_res = match GameResult::from_str(splitter.1) {
        Some(value) => value,
        None => return Err(LineProcessError::ValueError)
    };
    // Now let's find out what am I playing
    let me = game_res.find_me(him);
    let me_points = me.points();
    let game_points = game_res.points();
    //println!("{}+{}={}", me_points, game_points, me_points+game_points);
    //println!("{}", me.points());
    //println!("{}", game_res.points());
    Ok(me_points + game_points)
}

fn main() {
    let mut f = File::open("input.txt").expect("Failed to open the input file");
    let mut buf = String::new();
    f.read_to_string(&mut buf).expect("Reading the file failed");
    let sum: u64 = buf.lines().map(|line| process_row(String::from(line)).unwrap() as u64).sum();
    println!("Sum is {}", sum);
    /*println!("1: {}", process_row(String::from("A Y")).unwrap());
    println!("2: {}", process_row(String::from("A X")).unwrap());
    println!("3: {}", process_row(String::from("A Z")).unwrap());
    println!("1: {}", process_row(String::from("A Y")).unwrap());
    println!("2: {}", process_row(String::from("B X")).unwrap());
    println!("3: {}", process_row(String::from("C Z")).unwrap());*/
    
}
