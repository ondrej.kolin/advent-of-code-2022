use std::{fmt, fs::File, io::Read};

pub enum Me {
    X,
    Y,
    Z,
}

impl Me {
    fn from_str(value: &str) -> Option<Me> {
        match value.trim() {
            "X" => Some(Me::X),
            "Y" => Some(Me::Y),
            "Z" => Some(Me::Z),
            _ => None
        }
    }

    fn points(self: &Self) -> u8{
        match self {
            Self::X => 1,
            Self::Y => 2,
            Self::Z => 3
        }
    }
}

pub enum Him {
    A,
    B,
    C,
}

impl Him {
    fn from_str(value: &str) -> Option<Him> {
        match value.trim() {
            "A" => Some(Him::A),
            "B" => Some(Him::B),
            "C" => Some(Him::C),
            _ => None
        }
    }
}

#[derive(Debug)]
enum LineProcessError {
    EmptyLine,
    ParseError,
    ValueError,
}

impl fmt::Display for LineProcessError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            LineProcessError::ParseError => write!(f, "Unable to parse the line!"),
            LineProcessError::EmptyLine => write!(f, "It's an empty line"),
            LineProcessError::ValueError => write!(f, "Value error!")
        }
    }
}

fn compete(him: &Him, me: &Me) -> u8 {
    let res: u8 = match him { 
        Him::A => match me {
            Me::X => 1,
            Me::Y => 2,
            Me::Z => 0
        },
        Him::B => match me {
            Me::Y => 1,
            Me::Z => 2,
            Me::X => 0
        },
        Him::C => match me {
            Me::Z => 1,
            Me::X => 2,
            Me::Y => 0,
        }
    };
    res * 3
}

fn process_row(value: String) -> Result<u8, LineProcessError> {
    let cleaned = value.trim();
    if cleaned.is_empty() {
        return Err(LineProcessError::EmptyLine);
    }
    let mid = cleaned.find(" ");
    let splitter = match mid {
        None => return Err(LineProcessError::ParseError),
        Some(mid) => cleaned.split_at(mid)
    };
    let him = match Him::from_str(splitter.0) {
        Some(value) => value,
        None => return Err(LineProcessError::ValueError)
    };
    let me = match Me::from_str(splitter.1) {
        Some(value) => value,
        None => return Err(LineProcessError::ValueError)
    };
    let points = me.points() + compete(&him, &me);
    Ok(points)
}

fn main() {
    let mut f = File::open("input.txt").expect("Failed to open the input file");
    let mut buf = String::new();
    f.read_to_string(&mut buf).expect("Reading the file failed");
    let sum: u64 = buf.lines().map(|line| process_row(String::from(line)).unwrap() as u64).sum();
    println!("Sum is {}", sum);
    
}
