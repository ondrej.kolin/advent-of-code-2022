use std::fs::File;
use std::io::{BufReader, Read};
use std::collections::{VecDeque, HashSet};



fn main() {
    let f = BufReader::new(File::open("input.txt").unwrap());
    let mut the_queue: VecDeque<u8> = VecDeque::new();
    let mut the_set: HashSet<u8> = HashSet::new();
    for (index, b) in f.bytes().enumerate() {
        let c = b.unwrap();
        println!("#{index} - Found {}", c as char);
        if !the_set.contains(&c) {
            //Extend both queue and the set
            println!("Adding");
        }
        else {
            //We found a duplicate
            while let Some(r) = the_queue.pop_front() {
                println!("Removing {r}");
                the_set.remove(&r);
                // If the duplicate is taken out, stop clearing
                if r == c {
                    break;
                }
            }
        }
        the_set.insert(c);
        the_queue.push_back(c);
        if the_queue.len() == 14 {
            println!("End of the game {} {}", the_queue.iter().map(|x| *x as char).collect::<String>(), index + 1);
            return;
        }
        print!("Q({})", the_queue.len());
        for i in the_queue.iter() {
            print!(" {}", *i as char);
        }
        print!("\nS({})", the_set.len());
        for i in the_set.iter() {
            print!(" {}", *i as char);
        }
        println!("\n-------------");
    }
}

