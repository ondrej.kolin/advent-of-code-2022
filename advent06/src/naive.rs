use std::collections::hash_map::RandomState;
use std::fs::File;
use std::io::{BufRead, BufReader, Read};
use std::collections::{VecDeque, HashSet};



fn main() {
    let f = BufReader::new(File::open("input.txt").unwrap());
    let mut the_queue: VecDeque<u8> = VecDeque::new();
    for (index, b) in f.bytes().enumerate() {
        let c = b.unwrap();
        the_queue.push_back(c);
        println!("Added {c}");
        if the_queue.len() == 14 {
            let set: HashSet<u8, RandomState> = HashSet::from_iter(the_queue.iter().cloned());
            if set.len() == 14 {
                println!("Found it on {index} - ingame it's {}", index+1);
                return;
            }
            let removed = the_queue.pop_front().unwrap();
            println!("Removed {removed}");
        }
    }
}
