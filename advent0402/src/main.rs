use std::fs::File;
use std::io::{self, Read};

use regex::Regex;

fn process_plan(plan: [u16; 4]) -> bool{
    // If the other plan starts after the first one and ends before the first one it's inside
    //5-7, 7-9
    let first_one = plan[2] >= plan[0] && plan[2] <= plan[1];
    let second_one = plan[0] >= plan[2] && plan[0] <= plan[3];
    first_one || second_one
}

fn main() {
    let mut f = File::open("input.txt").expect("Unable to open the source file");
    let mut buf: String = String::new();
    f.read_to_string(&mut buf).expect("Unable to read the file");
    let re = Regex::new(r"(\d+)-(\d+),(\d+)-(\d+)").expect("x");
    let mut sum: u16 = 0;
    for line in buf.lines() {
        let caps = re.captures(line).expect("Capture failed");
        if caps.len() != 5 {
            println!("{}", line);
            panic!("Unable to read the plan for the line")
        }
        let plan = [
            caps.get(1),
            caps.get(2),
            caps.get(3),
            caps.get(4)
        ].map(|x| x.unwrap().as_str().to_string().parse::<u16>().unwrap());
        let res = process_plan(plan);
        sum += res as u16;
        //println!("{} {}", line, res);
    }
    println!("Sum: {}", sum)
}
