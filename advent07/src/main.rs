use std::cell::RefCell;
use std::rc::{Rc, Weak};

pub struct Tree {
    root: NodeLink,
}

impl Tree {
    fn new() -> Self {
        Tree {
            root: Rc::new(RefCell::new(Node {
                name: String::from("/"),
                children: vec![],
                parent: None
            }))
        }
    }
}

type NodeLink = Rc<RefCell<Node>>;

#[derive(Debug)]
struct Node {
    name: String,
    children: Vec<NodeLink>,
    parent: Option<Weak<RefCell<Node>>>,
}

impl Node {
    fn new(dir_name: &str) -> Self {
        Node {
            name: String::from(dir_name),
            parent: None,
            children: Vec::new()
        }
    }

    fn mkdir(self: &Self, name: &str) {
        let node = Node::new(name);
        self.children.push(node)
    }
}

fn main() {
    let t = Tree::new();
    
}