use std::{hash, collections::HashSet, borrow::BorrowMut, fs::File, io::Read};

fn char_to_index(c: &char) -> Option<u8> {
    let z = c.clone();
    match c {
        'a'..='z' => Some(z as u8 - 'a' as u8),
        'A'..='Z' => Some(z as u8 - 'A' as u8 + 26),
        _ => None
    }
}

fn process_group(buffer: [String; 3]) -> u8{
    let mut sets = Vec::from(buffer.map(|src| src.chars().into_iter().collect::<HashSet<char>>()));
    let mut the_set = sets.pop().expect("There are no hashsets!");
    sets.iter().for_each(|set| the_set.retain(|x| set.contains(x)));
    char_to_index(the_set
        .iter()
        .next()
        .unwrap())
    .unwrap() + 1
}

fn main() {
    //let x = String::from("Hello, world!");

    //let col  = x.chars().into_iter().collect();
    //let x = chars.into_iter().collect() as HashSet<char>;
    //.sum()
    //let buffer = [String::from("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"), String::from("ttgJtRGJQctTZtZT"), String::from("CrZsJsPPZsGzwwsLwLmpwMDw")];
    //println!("{}", process_group(buffer));
    let mut f = File::open("input.txt").expect("Unable to open the file");
    let mut buf = String::new();
    f.read_to_string(&mut buf).expect("Error reading the file");
    let mut lines = buf.lines();
    let mut sum: u64 = 0;
    loop {
        let first = lines.next();
        sum += match first {
            Some(first) => process_group([
                String::from(first),
                String::from(lines.next().unwrap()),
                String::from(lines.next().unwrap())
            ]),
            None => break
        } as u64
    };
    println!("Sum {sum}");

}
