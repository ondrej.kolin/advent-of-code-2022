use std::{fs::File, fmt::{Error, self}, io::Read};

fn char_to_index(c: char) -> Option<u8> {
    match c {
        'a'..='z' => Some(c as u8 - 'a' as u8),
        'A'..='Z' => Some(c as u8 - 'A' as u8 + 26),
        _ => None
    }
}

fn process_line(line: &String) -> u64 {
    let mut was_there: [u8; 52] = [0; 52];
    let mut sum: u64 = 0;
    let half = &line[0..line.len()/2];
    half.chars().for_each(|c| was_there[char_to_index(c).unwrap() as usize] += 1);
    let other_half = &line[line.len()/2..line.len()];
    for c in other_half.chars() {
        let index = char_to_index(c).unwrap() as usize;
        if was_there[index] > 0 {
            sum += index as u64 + 1;
            was_there[index] = 0;
        }
    }
    println!("{} - {}", line, sum);
    sum
}

fn main() {
    let mut f = File::open("input.txt").expect("Unable to open the file");
    let mut buf = String::new();
    f.read_to_string(&mut buf).expect("Error reading the file");
    let res: u64 = buf.lines().map(|line| process_line(&String::from(line))).sum();
    //buf.lines().filter(|line| line.len() % 2 == 1).for_each(|l| println!("{}", l))
    println!("{}", res)
    //let val = process_line(&String::from("vJrwpWtwJgWrhcsFMMfFFhFp")).unwrap();
}
